unit main_f;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CPortCtl, StdCtrls, Buttons, ExtCtrls, CPort, sc_Interface,
  Spin, TeEngine, Series, TeeProcs, Chart, ComCtrls, Spectrograph, ComObj, Excel2000,
  gGaussList, RXSpin, Grids, Spectrum, Placemnt, ActnMan, ActnColorMaps, Math, SpectroStorage;

type
  TForm1 = class(TForm)
    prt: TComPort;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ComLed1: TComLed;
    BitBtn2: TBitBtn;
    tmrMain: TTimer;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    sbar: TStatusBar;
    btnStart: TButton;
    edtPos: TSpinEdit;
    btnGo: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    chart: TChart;
    Series2: TPointSeries;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    edtFirstPos: TSpinEdit;
    edtLastPos: TSpinEdit;
    edtStep: TSpinEdit;
    Button1: TButton;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    sgScale: TStringGrid;
    edtX: TRxSpinEdit;
    edtY: TRxSpinEdit;
    BitBtn9: TBitBtn;
    edtTEst: TRxSpinEdit;
    edtResult: TRxSpinEdit;
    BitBtn10: TBitBtn;
    GroupBox2: TGroupBox;
    cbx1200: TRadioButton;
    FormStorage1: TFormStorage;
    Series3: TPointSeries;
    cbx600: TRadioButton;
    cbx1: TRadioButton;
    cbxFullScale: TCheckBox;
    Settings: TTabSheet;
    GroupBox3: TGroupBox;
    edtMinLen: TSpinEdit;
    edtMaxLen: TSpinEdit;
    GroupBox4: TGroupBox;
    CheckBox1: TCheckBox;
    edtMarkerPos: TSpinEdit;
    lblMarker: TLabel;
    ledDone: TComLed;
    ledReady: TComLed;
    btnAdd: TBitBtn;
    Series4: TLineSeries;
    Series13: TLineSeries;
    Series14: TFastLineSeries;
    cbxMeasure: TCheckBox;
    lblFreq: TLabel;
    tmrFreq: TTimer;
    lblFreq2: TLabel;
    BitBtn13: TBitBtn;
    GroupBox5: TGroupBox;
    edtCutOff: TRxSpinEdit;
    BitBtn12: TBitBtn;
    cbxPen: TCheckBox;
    BitBtn14: TBitBtn;
    lblPos1: TLabel;
    lblPos2: TLabel;
    Series1: TLineSeries;
    Series5: TLineSeries;
    Series6: TLineSeries;
    Series7: TLineSeries;
    Series8: TLineSeries;
    Series9: TLineSeries;
    Series10: TLineSeries;
    Series11: TLineSeries;
    Series12: TLineSeries;
    GroupBox6: TGroupBox;
    edtDarkCurrent: TSpinEdit;
    BitBtn8: TBitBtn;
    cbxDarkCurrent: TCheckBox;
    edtDarkSum: TSpinEdit;
    edtDarkCount: TSpinEdit;
    cbx1200_1: TRadioButton;
    cbx600_1: TRadioButton;
    BitBtn11: TBitBtn;
    Label4: TLabel;
    edtFilter: TSpinEdit;
    Label5: TLabel;
    edtSpeed: TSpinEdit;
    Label6: TLabel;
    lblCharCount: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure prtRxChar(Sender: TObject; Count: Integer);
    procedure tmrMainTimer(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure Spectrograph1FinishMeasure(pos, value: Double);
    procedure btnMeasureClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtStepChange(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure cbx1Click(Sender: TObject);
    procedure cbxFullScaleClick(Sender: TObject);
    procedure chartMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edtMarkerPosChange(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure cbxMeasureClick(Sender: TObject);
    procedure tmrFreqTimer(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure edtDarkCurrentChange(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
  private
    { Private declarations }
  public
    parser : TSParser;
    dir : Integer;
    isMove : Boolean;
    scaler : TScaler;
    DR1200 : TDR1200;
    DR600 : TDR600;
    DR1 : TDR1;

    DRApparatus600 : TDRApparatus600;
    DRApparatus1200 : TDRApparatus1200;
    DRApparatus1 : TDRApparatus1;
    curSeries : Integer;

    waitCmdIndex : Integer;

    procedure SendCommand( c : Char; val : Integer );
    procedure ChangeDR;
    procedure SetGraphWidth;
    procedure RepaintGraph;
    function getY( x : Integer ) : Integer;
  end;

var
  Form1: TForm1;

  storage : TSList;
  prevValue : Integer;
  charCount : Integer;
const
  FIRSTSERIESNUMBER = 5;
  MARKER_INDEX = 4;
  POINTER_INDEX = 3;

implementation


{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  prt.Open;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  prt.ShowSetupDialog;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Excel : Variant;
  i,j, curSer : Integer;
  oWB, oSheet, oChart, oCharts : OLEVariant;
  XRange : Variant;
begin
   Excel := CreateOleObject('Excel.Application');
   oWB := Excel.WorkBooks.Add; //������� ����� �������
   oSheet := oWB.ActiveSheet;
   Excel.Visible := True;
   Excel.Range['A1', 'Z1'].Font.FontStyle := 'Bold';

   for j := FIRSTSERIESNUMBER to curSeries do begin
     curSer := 1 + j - FIRSTSERIESNUMBER;
     Excel.ActiveSheet.Cells.Item[1,1 + curSer * 3].Value := 'Position:';
     Excel.ActiveSheet.Cells.Item[1,2 + curSer * 3].Value := 'Rate, Hz';
     Excel.ActiveSheet.Cells.Item[1,3 + curSer * 3].Value := chart.Series[j].Title;

     for i := 0 to chart.Series[j].Count - 1 do begin
       Excel.ActiveSheet.Cells.Item[i + 2, 1 + curSer * 3].Value := chart.Series[j].XValue[i];
       Excel.ActiveSheet.Cells.Item[i + 2, 2 + curSer * 3].Value := chart.Series[j].YValue[i];
     end;
   end;

{   
   for i := 0 to 50 do begin
     Excel.ActiveSheet.Cells.Item[i + 2, 1].Value := i*2;
     Excel.ActiveSheet.Cells.Item[i + 2, 2].Value := i*i;
   end;
{}

{
   oChart := oWB.Charts.Add(EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;
//   oChart.ChartWizard(oSheet.Range['A1','B99'], xlXYScatterLines, EmptyParam,
//                                xlRows,
//                                EmptyParam, EmptyParam, EmptyParam,
//                                EmptyParam, EmptyParam,
//                                EmptyParam, EmptyParam);
//
//
   XRange := oSheet.Range['A2:A99'] ;
   oChart.SetSourceData( oSheet.Range['A2','B99'], xlColumns );
   oChart.SeriesCollection[1].XValues := XRange;
   oChart.Name:='Luminescense spectum';
   oChart.HasTitle:=True;
   oChart.ChartTitle.Characters.Text:='Luminescense spectum graph';
   oChart.ChartType := xlXYScatterLines;
   }
end;

procedure TForm1.btnStartClick(Sender: TObject);
var
  smsg : String;
begin
  if( edtPos.Text <> '' ) then begin
    SendCommand( 'g', Round( scaler.GetPos(edtPos.Value) ) );
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  storage := TSList.Create;

  DR1 := TDR1.Create;
  DR600 := TDR600.Create;
  DR1200 := TDR1200.Create;
  DRApparatus600 := TDRApparatus600.Create;
  DRApparatus1200 := TDRApparatus1200.Create;
  DRApparatus1 := TDRApparatus1.Create;

  parser := TSParser.Create;

  try
    prt.Open;
  except
    ShowMessage('������ �������� ����� �� ���������. ��������� ����������� ��������� � �������� ��������������� ����');
  end;

  curSeries := FIRSTSERIESNUMBER-1;

  btnAdd.Click;

  ChangeDR;


  //chart.Series[0].AddXY( 0, 0 );
  chart.Series[0].AddXY(0, 0);
  chart.Series[0].AddXY(10000, 0);

  chart.Series[1].AddXY( 2000, 0 );
  chart.Series[1].AddXY( 6250, 0 );

  chart.Series[2].AddXY( 4000, 0 );
  chart.Series[2].AddXY( 12500, 0 );
  //chart.Series[0].FillSampleValues( 10 );
end;

procedure TForm1.prtRxChar(Sender: TObject; Count: Integer);
var
  chr : array [0..1] of Byte;
begin
  while( prt.InputCount > 0 ) do begin
    if( 1 = prt.Read( chr, 1 ) ) then begin
      parser.PushChar( chr[0] );
      inc( charCount );
      lblCharCount.Caption := IntToStr( charCount );
    end;
  end;
end;

procedure TForm1.tmrMainTimer(Sender: TObject);
var
  a : Char;
begin

  if( parser.IsNewCmd ) then begin
    if( parser.IsDrawPoint ) then begin
      if cbxPen.Checked then begin
        Storage.GetSpectrum( storage.Count - 1 ).AddPoint( parser.LastPosition, parser.LAstValue );
      end;
      //chart.series[curSeries].AddXY( scaler.GetWave( parser.LastPosition ), parser.LAstValue );
    end;


    chart.Series[ POINTER_INDEX ].Clear;
    chart.Series[POINTER_INDEX].AddXY( scaler.GetWave( parser.LastPosition), 0 );
    sbar.Panels[1].Text := IntToStr( parser.LastValue );
    lblFreq.Caption := Format('%6.0d.', [parser.LAstValue div 1000] );
    lblFreq2.Caption := Format('%3.3d', [parser.LAstValue mod 1000] );
    sbar.Panels[3].Text := IntToStr( parser.LastPosition );
    sbar.Panels[5].Text := IntToStr( Round( scaler.GetWave( parser.LastPosition ) ));
    sbar.Panels[7].Text := '';
    sbar.Panels[9].Text := IntToStr( parser.LastHVoltage );
    sbar.Panels[11].Text := IntToStr( parser.LastCutOff );
    ledDone.State := lsOff;
    ledReady.State := lsOff;

    if( cbxDarkCurrent.Checked )then begin
      if( prevValue <> parser.LastValue ) then begin
        edtDarkCount.Value := edtDarkCount.Value + 1;
        edtDarkSum.Value := edtDarkSum.Value + parser.LastValue;
        edtDarkCurrent.Value := Round(edtDarkSum.Value / edtDarkCount.Value);
        prevValue := parser.LastValue;
      end;
    end;

    if( parser.IsGoToHome ) then sbar.Panels[7].Text := sbar.Panels[7].Text + ' GoToHome |';
    if( parser.IsHome ) then sbar.Panels[7].Text := sbar.Panels[7].Text + ' HOME |';
    if( parser.IsEnd ) then sbar.Panels[7].Text := sbar.Panels[7].Text + ' END |';
    if( parser.IsDone ) then begin
      sbar.Panels[7].Text := sbar.Panels[7].Text + ' DONE |';
      ledDone.State := lsOn;
    end;
    if( parser.IsReady ) then begin
      sbar.Panels[7].Text := sbar.Panels[7].Text + ' READY';
      ledReady.State := lsOn;
    end;
    //parser.IsNewCmd := false;
  end else begin
    //prt.Write(a, 1);
  end;
end;

procedure TForm1.btnGoClick(Sender: TObject);
begin
  chart.Series[curSeries].Clear;
  //chart.Series[curSeries].AddXY( 0, 0 );
end;

procedure TForm1.Spectrograph1FinishMeasure(pos, value: Double);
begin
  ShowMessage( 'OnFinish: ' + FLoatToStr( pos ) );
end;

procedure TForm1.btnMeasureClick(Sender: TObject);
var
  smsg : String;
begin
  smsg := Format( 'm=%.5d' + #10, [edtPos.Value]);
  prt.WriteStr(smsg);

end;

procedure TForm1.BitBtn3Click(Sender: TObject);
var
  smsg : String;
begin
  smsg := Format( 'h=%.5d' + #10, [edtPos.Value]);
  prt.WriteStr(smsg);
end;

procedure TForm1.SendCommand(c: Char; val: Integer);
var
  smsg : String;
begin
  smsg := Format( c + '=%.5d' + #10, [val]);
  prt.WriteStr(smsg);
end;

procedure TForm1.BitBtn5Click(Sender: TObject);
begin
  SendCommand( 's', 0 );
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  tmrMain.Enabled := false;
  Sleep(300);
  Application.ProcessMessages;
  prt.Close;
  canClose := true;
end;

procedure TForm1.BitBtn6Click(Sender: TObject);
begin
  lblPos1.Caption := IntToStr( Round( scaler.GetPos(edtFirstPos.Value) / 32 ) * 32 );
  lblPos2.Caption := IntToStr( Round( scaler.GetPos(edtLastPos.Value) ) );

  SendCommand( 'F', Round( scaler.GetPos(edtFirstPos.Value) ) );
  Sleep(100);
  SendCommand( 'L', Round( scaler.GetPos(edtLastPos.Value) ) );
  Sleep(100);
  SendCommand( 'S', edtStep.Value );
  Sleep(100);
  SendCommand( 'f', edtFilter.Value );
  Sleep(100);
  SendCommand( 'E', 0 );

  SetGraphWidth;

  cbxPen.Checked := true;
end;

procedure TForm1.BitBtn9Click(Sender: TObject);
begin
  scaler.AddScalePoint( edtX.Value, edtY.Value );
  scaler.PrintToStringGrid( sgScale );
end;

procedure TForm1.BitBtn10Click(Sender: TObject);
begin
  edtResult.value := scaler.GetWave( edtTEst.Value );
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ShowMessage( IntToStr( Key ) );
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ShowMessage( IntToStr( Key ) );
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
  ShowMessage( Key );
end;

procedure TForm1.edtStepChange(Sender: TObject);
begin
  if( edtStep.Text <> '' ) then begin
    edtPos.Increment := edtStep.Value;
  end;
end;

procedure TForm1.BitBtn7Click(Sender: TObject);
begin
    SendCommand( 's', 0 );
end;

procedure TForm1.cbx1Click(Sender: TObject);
begin
  ChangeDR;
end;

procedure TForm1.ChangeDR;
begin
  if( cbx1200.Checked ) then begin
    storage.GetSpectrum( storage.Count - 1 ).XScaler := DR1200;
    storage.GetSpectrum( storage.Count - 1 ).YScaler := DRApparatus1200;
//    scaler := DR1200;
  end else if cbx600.Checked then begin
    storage.GetSpectrum( storage.Count - 1 ).XScaler := DR600;
    storage.GetSpectrum( storage.Count - 1 ).YScaler := DRApparatus600;
//    scaler := DR600;
  end else if cbx1200_1.Checked then begin
    storage.GetSpectrum( storage.Count - 1 ).XScaler := DR1200;
    storage.GetSpectrum( storage.Count - 1 ).YScaler := DRApparatus1;
  end else if cbx600_1.Checked then begin
    storage.GetSpectrum( storage.Count - 1 ).XScaler := DR600;
    storage.GetSpectrum( storage.Count - 1 ).YScaler := DRApparatus1;
  end else begin
    storage.GetSpectrum( storage.Count - 1 ).XScaler := DR1;
    storage.GetSpectrum( storage.Count - 1 ).YScaler := DRApparatus1;
//    scaler := DR1;
  end;

  scaler := storage.GetSpectrum( storage.Count - 1 ).XScaler;
end;

procedure TForm1.cbxFullScaleClick(Sender: TObject);
begin
  SetGraphWidth;
end;

procedure TForm1.SetGraphWidth;
begin
  if( cbxFullScale.Checked ) then begin
    chart.BottomAxis.Minimum := edtMinLen.Value;
    chart.BottomAxis.Maximum := edtMaxLen.Value;
  end else begin
    chart.BottomAxis.Minimum := edtFirstPos.Value;
    chart.BottomAxis.Maximum := max(chart.BottomAxis.Minimum + 10, edtLastPos.Value );
  end;
end;

procedure TForm1.RepaintGraph;
begin

end;

procedure TForm1.chartMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  waveLen : Integer;
  wlIndex : Integer;
begin
  waveLen := Round( chart.Series[0].XScreenToValue( x ) );

  edtMarkerPos.Value := waveLen;
end;

procedure TForm1.edtMarkerPosChange(Sender: TObject);
var
  xpos, ypos : Integer;
begin
  chart.Series[ MARKER_INDEX ].Clear;
  xpos := edtMarkerPos.Value;
  ypos := getY( xpos );
  chart.Series[MARKER_INDEX].AddXY( xpos, ypos );
  lblMarker.Caption := IntToStr( ypos );
end;

function TForm1.getY(x: Integer): Integer;
var
  i : Integer;
  xi, yi, xii, yii : Real;
begin
  for i := 0 to chart.series[curSeries].Count - 2 do begin
    if( x < chart.series[curSeries].XValue[i+1] ) then begin
      xi := chart.series[curSeries].XValue[i];
      yi := chart.series[curSeries].YValue[i];
      xii := chart.series[curSeries].XValue[i+1];
      yii := chart.series[curSeries].YValue[i+1];

      result := Round( yi + (x-xi)/(xii-xi)*(yii-yi) ) ;
      exit;
    end;
  end;
  result := 0;//Round( chart.series[curSeries].YValue[chart.series[curSeries].Count - 1] );
end;

procedure TForm1.btnAddClick(Sender: TObject);
var
  s : TSStorage;
begin
  if( chart.SeriesCount - 2 > curSeries ) then inc( curSeries ) else begin
    ShowMessage('��� ��������� �����');
    exit;
  end;

  if( cbx1200.Checked ) then begin
    s := TSStorage.Create( DR1200, DR1 );
    s.Caption := 'DR1200';
    chart.Series[curSeries].Title := 'DR1200';
  end else if ( cbx600.Checked ) then begin
    s := TSStorage.Create( DR600, DR1 );
    s.Caption := 'DR600';
    chart.Series[curSeries].Title := 'DR600';
  end else begin
    s := TSStorage.Create( DR1, DR1 );
    s.Caption := '1:1';
    chart.Series[curSeries].Title := '1:1';
  end;

  s.ChartSeries := chart.Series[curSeries];

  storage.AddSpectrum( s );

  ChangeDR;
end;

procedure TForm1.BitBtn12Click(Sender: TObject);
begin
  SendCommand('c', Round( edtCutOff.Value ) );
end;

procedure TForm1.cbxMeasureClick(Sender: TObject);
begin
  tmrFreq.Enabled := cbxMeasure.Checked;
end;

procedure TForm1.tmrFreqTimer(Sender: TObject);
begin
  SendCommand('m', 0);
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
var
  t : Integer;
begin
  t := Random( 10000000 );
    lblFreq.Caption := Format('%6.0d.', [t div 1000] );
    lblFreq2.Caption := Format('%3.3d', [t mod 1000] );
end;

procedure TForm1.BitBtn14Click(Sender: TObject);
begin
  SendCommand( 'S', edtStep.Value );  
  //SendCommand( 'S', edtStep.Value );  
end;

procedure TForm1.BitBtn8Click(Sender: TObject);
begin
  edtDarkSum.Value := 0;
  edtDarkCount.Value := 0;
end;

procedure TForm1.edtDarkCurrentChange(Sender: TObject);
begin
  storage.GetSpectrum( storage.Count - 1 ).DarkCurrent := edtDarkCurrent.Value ;
end;

procedure TForm1.BitBtn11Click(Sender: TObject);
begin
  lblPos1.Caption := IntToStr( Round( scaler.GetPos(edtFirstPos.Value) / 32 ) * 32 );
  lblPos2.Caption := IntToStr( Round( scaler.GetPos(edtLastPos.Value) ) );

  SendCommand( 'F', Round( scaler.GetPos(edtFirstPos.Value) ) );
  Sleep(100);
  SendCommand( 'L', Round( scaler.GetPos(edtLastPos.Value) ) );
  Sleep(100);
  SendCommand( 'f', edtFilter.Value );
  Sleep(100);
  SendCommand( 'S', edtSpeed.Value );
  Sleep(100);
  SendCommand( 'e', 0 );

  SetGraphWidth;

  cbxPen.Checked := true;
end;

end.
