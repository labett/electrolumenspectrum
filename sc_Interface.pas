unit sc_Interface;

interface

const
  PRE = ord('s');
  MSG_SIZE = 19;

  
  FLAG_HOME = 1;
  FLAG_END = 2;
  FLAG_DONE  = 4;
  FLAG_GOTOHOME  = 8;
  FLAG_READY = 16;
  FLAG_HOMESENSORERROR = (32);
  FLAG_ENGINEERROR =(64);
  FLAG_DRAWPOINT = 128;


type
TSParser = class
  private
    fIsPreambleReceived : Boolean;
    fMessageIx : Integer;
    fCRC : Byte;
    fMessage : array[0..MSG_SIZE] of byte;
    fIsNewCmd: Boolean;
    lastPos, lastVal : Integer;
    curState : Integer;
    fLastHVoltage: Integer;
    fLastCutOff: Integer;

    procedure ParseMessage;
    function getIsNewCmd: Boolean;
    function getIsHome: Boolean;
    function getIsDone: Boolean;
    function getIsEnd: Boolean;
    function getIsGoToHome: Boolean;
    function getIsReady: Boolean;
    function getIsDrawPoint: Boolean;
  public
    procedure PushChar( b : Byte );

    property IsNewCmd : Boolean read getIsNewCmd;
    property LastPosition : Integer read lastPos;
    property LAstValue : Integer read lastVal;
    property LastHVoltage : Integer read fLastHVoltage;
    property LastCutOff : Integer read fLastCutOff;
    property IsHome : Boolean read getIsHome;
    property IsEnd : Boolean read getIsEnd;
    property IsDone : Boolean read getIsDone;
    property IsGoToHome : Boolean read getIsGoToHome;
    property IsReady : Boolean read getIsReady;
    property IsDrawPoint : Boolean read getIsDrawPoint;

end;



implementation

{ TSParser }

function TSParser.getIsDone: Boolean;
begin
  result := ( curState and FLAG_DONE ) > 0;
end;

function TSParser.getIsDrawPoint: Boolean;
begin
  result := ( curState and FLAG_DRAWPOINT ) > 0;
end;

function TSParser.getIsEnd: Boolean;
begin
  result := ( curState and FLAG_END ) > 0;
end;

function TSParser.getIsGoToHome: Boolean;
begin
  result := ( curState and FLAG_GOTOHOME ) > 0;
end;

function TSParser.getIsHome: Boolean;
begin
  result := ( curState and FLAG_HOME ) > 0;
end;

function TSParser.getIsNewCmd: Boolean;
begin
  result := fIsNewCmd;
  fIsNewCmd := false;
end;

function TSParser.getIsReady: Boolean;
begin
  result := ( curState and FLAG_READY ) > 0;
end;

procedure TSParser.ParseMessage;
begin
  if( $FF = fMessage[ MSG_SIZE - 1 ] ) then begin
    lastPos := (fMessage[5] shl 24 ) + (fMessage[6] shl 16 )  + (fMessage[7] shl 8 ) + (fMessage[8] );
    lastVal := (fMessage[9] shl 24 ) + (fMessage[10] shl 16 )  + (fMessage[11] shl 8 ) + (fMessage[12] );
    curState := fMessage[13] ;
    fLastHVoltage :=  (fMessage[14] shl 8 ) + (fMessage[15] );
    fLastCutOff :=  (fMessage[16] shl 8 ) + (fMessage[17] );
    fIsNewCmd := true;
  end;
end;

procedure TSParser.PushChar(b: Byte);
begin
  if( fIsPreambleReceived ) then begin
    fMessage[ fMessageIx ] := b;
    inc( fCrc, b );
    inc( fMessageIx );
    if( fMessageIx >= MSG_SIZE ) then begin
      ParseMessage;
      fIsPreambleReceived := false;
    end;
  end else begin
    if( b = PRE ) then begin
      fIsPreambleReceived := true;
      fMessageIx := 1;
    end;
  end;
end;

end.
