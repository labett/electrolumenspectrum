object Form1: TForm1
  Left = 214
  Top = 126
  Width = 1056
  Height = 813
  Caption = #1057#1087#1077#1082#1090#1088#1086#1075#1088#1072#1092
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 88
    Width = 40
    Height = 13
    Caption = #1060#1080#1083#1100#1090#1088
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1048
    Height = 41
    Align = alTop
    TabOrder = 0
    object ComLed1: TComLed
      Left = 640
      Top = 8
      Width = 25
      Height = 25
      ComPort = prt
      LedSignal = lsConn
      Kind = lkRedLight
    end
    object ledDone: TComLed
      Left = 880
      Top = 8
      Width = 25
      Height = 25
      LedSignal = lsConn
      Kind = lkYellowLight
    end
    object ledReady: TComLed
      Left = 904
      Top = 8
      Width = 25
      Height = 25
      LedSignal = lsConn
      Kind = lkGreenLight
    end
    object BitBtn1: TBitBtn
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Settings'
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 680
      Top = 8
      Width = 57
      Height = 25
      Caption = 'HOME'
      TabOrder = 2
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 736
      Top = 8
      Width = 57
      Height = 25
      Caption = 'END'
      TabOrder = 3
    end
    object BitBtn5: TBitBtn
      Left = 808
      Top = 8
      Width = 57
      Height = 25
      Caption = 'DONE'
      TabOrder = 4
      OnClick = BitBtn5Click
    end
    object btnStart: TButton
      Left = 248
      Top = 8
      Width = 75
      Height = 25
      Caption = 'GoTo ->'
      TabOrder = 5
      OnClick = btnStartClick
    end
    object edtPos: TSpinEdit
      Left = 328
      Top = 11
      Width = 121
      Height = 22
      MaxValue = 999999
      MinValue = 0
      TabOrder = 6
      Value = 0
    end
    object btnGo: TBitBtn
      Left = 456
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Clear'
      TabOrder = 7
      OnClick = btnGoClick
    end
    object Button1: TButton
      Left = 536
      Top = 8
      Width = 75
      Height = 25
      Caption = #1069#1082#1089#1077#1083#1100
      TabOrder = 8
      OnClick = Button1Click
    end
  end
  object sbar: TStatusBar
    Left = 0
    Top = 760
    Width = 1048
    Height = 19
    Panels = <
      item
        Text = 'Freq:'
        Width = 30
      end
      item
        Width = 50
      end
      item
        Text = 'Position:'
        Width = 50
      end
      item
        Width = 100
      end
      item
        Text = 'WaveLen'
        Width = 60
      end
      item
        Width = 50
      end
      item
        Text = 'STATUS'
        Width = 50
      end
      item
        Width = 200
      end
      item
        Text = 'HVoltage'
        Width = 60
      end
      item
        Width = 50
      end
      item
        Text = 'CutOff'
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 1048
    Height = 719
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet3: TTabSheet
      Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1087#1088#1086' '#1086#1073#1088#1072#1079#1077#1094
      ImageIndex = 2
    end
    object TabSheet1: TTabSheet
      Caption = #1043#1088#1072#1092#1080#1082
      object chart: TChart
        Left = 0
        Top = 0
        Width = 839
        Height = 691
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          '')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.Maximum = 6500.000000000000000000
        Legend.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 0
        OnMouseDown = chartMouseDown
        object Series14: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clWhite
          LinePen.Color = clWhite
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series13: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 8454143
          LinePen.Color = 8454143
          LinePen.Width = 3
          Pointer.Brush.Color = 8454143
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = -1
          Pointer.Style = psRectangle
          Pointer.VertSize = 5
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series4: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 8454016
          LinePen.Color = clBlue
          LinePen.Style = psDot
          LinePen.Width = 2
          Pointer.Brush.Color = 8454016
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.VertSize = 5
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series2: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clGreen
          Pointer.Brush.Color = clRed
          Pointer.HorizSize = 9
          Pointer.InflateMargins = True
          Pointer.Style = psTriangle
          Pointer.VertSize = 15
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series3: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clGreen
          Pointer.Brush.Color = 16744448
          Pointer.HorizSize = 3
          Pointer.InflateMargins = True
          Pointer.Style = psCircle
          Pointer.VertSize = 3
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 8404992
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series5: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series6: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clBlack
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series7: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 4227072
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series8: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 16711808
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series9: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 4227327
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series10: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clFuchsia
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series11: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 8454016
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series12: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clBlack
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object GroupBox1: TGroupBox
        Left = 839
        Top = 0
        Width = 201
        Height = 691
        Align = alRight
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072
        TabOrder = 1
        DesignSize = (
          201
          691)
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 83
          Height = 13
          Caption = #1053#1072#1095'. '#1055#1086#1083#1086#1078#1077#1085#1080#1077
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 83
          Height = 13
          Caption = #1050#1086#1085'. '#1055#1086#1083#1086#1078#1077#1085#1080#1077
        end
        object Label3: TLabel
          Left = 8
          Top = 64
          Width = 20
          Height = 13
          Caption = #1064#1072#1075
        end
        object lblPos1: TLabel
          Left = 136
          Top = 392
          Width = 34
          Height = 13
          Caption = 'lblPos1'
        end
        object lblPos2: TLabel
          Left = 136
          Top = 408
          Width = 34
          Height = 13
          Caption = 'lblPos1'
        end
        object Label4: TLabel
          Left = 8
          Top = 88
          Width = 40
          Height = 13
          Caption = #1060#1080#1083#1100#1090#1088
        end
        object Label6: TLabel
          Left = 8
          Top = 112
          Width = 48
          Height = 13
          Caption = #1057#1082#1086#1088#1086#1089#1090#1100
        end
        object lblCharCount: TLabel
          Left = 8
          Top = 136
          Width = 60
          Height = 13
          Caption = 'lblCharCount'
        end
        object BitBtn6: TBitBtn
          Left = 8
          Top = 625
          Width = 185
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = #1057#1090#1072#1088#1090' '#1089' '#1086#1089#1090#1072#1085#1086#1074#1082#1072#1084#1080
          TabOrder = 0
          OnClick = BitBtn6Click
          Kind = bkIgnore
        end
        object BitBtn7: TBitBtn
          Left = 120
          Top = 657
          Width = 75
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = #1057#1090#1086#1087
          TabOrder = 1
          OnClick = BitBtn7Click
          Kind = bkAbort
        end
        object edtFirstPos: TSpinEdit
          Left = 96
          Top = 11
          Width = 97
          Height = 22
          MaxValue = 25000
          MinValue = 0
          TabOrder = 2
          Value = 0
        end
        object edtLastPos: TSpinEdit
          Left = 96
          Top = 35
          Width = 97
          Height = 22
          MaxValue = 12000
          MinValue = 1
          TabOrder = 3
          Value = 6500
        end
        object edtStep: TSpinEdit
          Left = 96
          Top = 59
          Width = 97
          Height = 22
          MaxValue = 10000
          MinValue = 1
          TabOrder = 4
          Value = 100
          OnChange = edtStepChange
        end
        object GroupBox2: TGroupBox
          Left = 8
          Top = 216
          Width = 185
          Height = 129
          Caption = #1056#1077#1096#1077#1090#1082#1072
          TabOrder = 5
          object cbx1200: TRadioButton
            Left = 8
            Top = 16
            Width = 113
            Height = 17
            Caption = '1200'
            TabOrder = 0
            OnClick = cbx1Click
          end
          object cbx600: TRadioButton
            Left = 8
            Top = 32
            Width = 57
            Height = 17
            Caption = '600'
            TabOrder = 1
            OnClick = cbx1Click
          end
          object cbx1: TRadioButton
            Left = 8
            Top = 48
            Width = 113
            Height = 17
            Caption = '1:1'
            TabOrder = 2
            OnClick = cbx1Click
          end
          object cbxFullScale: TCheckBox
            Left = 8
            Top = 80
            Width = 137
            Height = 17
            Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1102' '#1096#1082#1072#1083#1091
            Checked = True
            State = cbChecked
            TabOrder = 3
            OnClick = cbxFullScaleClick
          end
          object btnAdd: TBitBtn
            Left = 104
            Top = 96
            Width = 75
            Height = 25
            Caption = #1044#1086#1073#1072#1074#1080#1090#1100
            TabOrder = 4
            OnClick = btnAddClick
          end
          object cbxPen: TCheckBox
            Left = 8
            Top = 104
            Width = 97
            Height = 17
            Caption = 'cbxPen'
            TabOrder = 5
          end
          object cbx1200_1: TRadioButton
            Left = 64
            Top = 16
            Width = 57
            Height = 17
            Caption = '1200:1'
            TabOrder = 6
            OnClick = cbx1Click
          end
          object cbx600_1: TRadioButton
            Left = 64
            Top = 32
            Width = 57
            Height = 17
            Caption = '600:1'
            Checked = True
            TabOrder = 7
            TabStop = True
            OnClick = cbx1Click
          end
        end
        object GroupBox4: TGroupBox
          Left = 8
          Top = 344
          Width = 177
          Height = 105
          Caption = #1052#1072#1088#1082#1077#1088
          TabOrder = 6
          object lblMarker: TLabel
            Left = 8
            Top = 64
            Width = 77
            Height = 24
            Caption = 'lblMarker'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -21
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CheckBox1: TCheckBox
            Left = 8
            Top = 16
            Width = 97
            Height = 17
            Caption = #1055#1086#1082#1072#1079#1072#1090#1100
            TabOrder = 0
          end
          object edtMarkerPos: TSpinEdit
            Left = 8
            Top = 35
            Width = 121
            Height = 22
            MaxValue = 999999
            MinValue = 0
            TabOrder = 1
            Value = 0
            OnChange = edtMarkerPosChange
          end
        end
        object BitBtn14: TBitBtn
          Left = 128
          Top = 144
          Width = 51
          Height = 25
          Caption = #1064#1072#1075' >>'
          TabOrder = 7
          OnClick = BitBtn14Click
        end
        object GroupBox6: TGroupBox
          Left = 8
          Top = 448
          Width = 177
          Height = 153
          Caption = #1058#1077#1084#1085#1086#1074#1086#1081' '#1090#1086#1082
          TabOrder = 8
          object edtDarkCurrent: TSpinEdit
            Left = 8
            Top = 19
            Width = 97
            Height = 22
            MaxValue = 999999
            MinValue = 0
            TabOrder = 0
            Value = 0
            OnChange = edtDarkCurrentChange
          end
          object BitBtn8: TBitBtn
            Left = 40
            Top = 64
            Width = 75
            Height = 25
            Caption = #1057#1095#1080#1090#1072#1090#1100
            TabOrder = 1
            OnClick = BitBtn8Click
          end
          object cbxDarkCurrent: TCheckBox
            Left = 8
            Top = 48
            Width = 97
            Height = 17
            Caption = #1053#1072#1082#1086#1087#1083#1077#1085#1080#1077
            TabOrder = 2
          end
          object edtDarkSum: TSpinEdit
            Left = 16
            Top = 91
            Width = 97
            Height = 22
            MaxValue = 999999
            MinValue = 0
            TabOrder = 3
            Value = 0
            OnChange = edtMarkerPosChange
          end
          object edtDarkCount: TSpinEdit
            Left = 16
            Top = 115
            Width = 97
            Height = 22
            MaxValue = 999999
            MinValue = 0
            TabOrder = 4
            Value = 0
            OnChange = edtMarkerPosChange
          end
        end
        object BitBtn11: TBitBtn
          Left = 8
          Top = 657
          Width = 75
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = #1057#1090#1072#1088#1090
          TabOrder = 9
          OnClick = BitBtn11Click
          Kind = bkIgnore
        end
        object edtFilter: TSpinEdit
          Left = 96
          Top = 83
          Width = 97
          Height = 22
          MaxValue = 10000
          MinValue = 1
          TabOrder = 10
          Value = 100
          OnChange = edtStepChange
        end
        object edtSpeed: TSpinEdit
          Left = 96
          Top = 107
          Width = 97
          Height = 22
          MaxValue = 10000
          MinValue = 1
          TabOrder = 11
          Value = 100
          OnChange = edtStepChange
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1048#1079#1084#1077#1088#1077#1085#1080#1077
      ImageIndex = 1
      object lblFreq: TLabel
        Left = 16
        Top = 48
        Width = 480
        Height = 150
        Alignment = taRightJustify
        Caption = '10000.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -133
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFreq2: TLabel
        Left = 488
        Top = 48
        Width = 240
        Height = 150
        Caption = '999'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -133
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbxMeasure: TCheckBox
        Left = 16
        Top = 8
        Width = 97
        Height = 17
        Caption = #1063#1072#1089#1090#1086#1090#1086#1084#1077#1088
        TabOrder = 0
        OnClick = cbxMeasureClick
      end
      object BitBtn13: TBitBtn
        Left = 528
        Top = 304
        Width = 75
        Height = 25
        Caption = 'BitBtn13'
        TabOrder = 1
        OnClick = BitBtn13Click
      end
      object GroupBox5: TGroupBox
        Left = 8
        Top = 232
        Width = 185
        Height = 73
        Caption = #1055#1086#1088#1086#1075
        TabOrder = 2
        object edtCutOff: TRxSpinEdit
          Left = 8
          Top = 16
          Width = 121
          Height = 21
          MaxValue = 4095.000000000000000000
          ValueType = vtHex
          TabOrder = 0
        end
        object BitBtn12: TBitBtn
          Left = 8
          Top = 40
          Width = 75
          Height = 25
          Caption = 'Set'
          TabOrder = 1
          OnClick = BitBtn12Click
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object sgScale: TStringGrid
        Left = 472
        Top = 24
        Width = 305
        Height = 401
        TabOrder = 0
      end
      object edtX: TRxSpinEdit
        Left = 88
        Top = 32
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object edtY: TRxSpinEdit
        Left = 88
        Top = 56
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object BitBtn9: TBitBtn
        Left = 232
        Top = 48
        Width = 75
        Height = 25
        Caption = 'BitBtn9'
        TabOrder = 3
        OnClick = BitBtn9Click
      end
      object edtTEst: TRxSpinEdit
        Left = 88
        Top = 152
        Width = 121
        Height = 21
        TabOrder = 4
      end
      object edtResult: TRxSpinEdit
        Left = 88
        Top = 176
        Width = 121
        Height = 21
        TabOrder = 5
      end
      object BitBtn10: TBitBtn
        Left = 224
        Top = 176
        Width = 75
        Height = 25
        Caption = 'BitBtn10'
        TabOrder = 6
        OnClick = BitBtn10Click
      end
    end
    object Settings: TTabSheet
      Caption = 'Settings'
      ImageIndex = 4
      object GroupBox3: TGroupBox
        Left = 8
        Top = 8
        Width = 185
        Height = 73
        Caption = #1050#1088#1072#1103' '#1091#1089#1090#1072#1085#1086#1074#1082#1080
        TabOrder = 0
        object edtMinLen: TSpinEdit
          Left = 8
          Top = 19
          Width = 97
          Height = 22
          MaxValue = 25000
          MinValue = 0
          TabOrder = 0
          Value = 0
        end
        object edtMaxLen: TSpinEdit
          Left = 8
          Top = 43
          Width = 97
          Height = 22
          MaxValue = 13000
          MinValue = 1
          TabOrder = 1
          Value = 12000
        end
      end
    end
  end
  object prt: TComPort
    BaudRate = br115200
    Port = 'COM2'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    StoredProps = [spBasic]
    TriggersOnRxChar = True
    OnRxChar = prtRxChar
    Left = 928
    Top = 8
  end
  object tmrMain: TTimer
    Interval = 50
    OnTimer = tmrMainTimer
    Left = 920
    Top = 40
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'edtFirstPos.Value'
      'edtLastPos.Value'
      'edtStep.Value'
      'cbx1.Checked'
      'cbx1200.Checked'
      'cbx1200_1.Checked'
      'cbx600.Checked'
      'cbx600_1.Checked'
      'edtFilter.Value')
    StoredValues = <>
    Left = 923
    Top = 417
  end
  object tmrFreq: TTimer
    Enabled = False
    Interval = 50
    OnTimer = tmrFreqTimer
    Left = 132
    Top = 305
  end
end
