unit SpectroStorage;

interface

uses Spectrum, Chart, Series, SysUtils, Classes, TeEngine;

type
  TSStorage = class
      raw : array of TRealPoint;
      view : array of TRealPoint;
      drawn : array of Boolean;

    private

      fCaption: String;
      fDate: TDateTime;
      fChartSeries: TChartSeries;
      fYScaler: TScaler;
      fXScaler: TScaler;
      fCount: Integer;
      fCapacity : Integer;
    fDarkCurrent: Real;

      procedure Draw;
      procedure Recalculate;

      procedure setChartSeries(const Value: TChartSeries);
      procedure setXScaler(const Value: TScaler);
      procedure setYScaler(const Value: TScaler);
    procedure setDarkCurrent(const Value: Real);

    public
      property Caption : String read fCaption write fCaption;
      property Date : TDateTime read fDate write fDate;
      property XScaler : TScaler read fXScaler write setXScaler;
      property YScaler : TScaler read fYScaler write setYScaler;
      property ChartSeries : TChartSeries read fChartSeries write setChartSeries;
      property Count : Integer read fCount;
      property DarkCurrent : Real read fDarkCurrent write setDarkCurrent;

      constructor Create( waveLenScaler, apparatusScaler : TScaler );

      function AddPoint( x, y : Real ) : Integer;
      function GetPoint( i : Integer ) : TRealPoint;
      function GetRawPoint( i : Integer ) : TRealPoint;

      //function
  end;

  TSList = class (TList)
    function AddSpectrum(Item: TSStorage): Integer;
    function GetSpectrum( ix : Integer ) : TSStorage;
  end;

implementation

{ TSStorage }

function TSStorage.AddPoint(x, y: Real): Integer;
//var
// tmpArray : array of T
begin
  inc( fCount, 1 );
  if( fCount > fCapacity ) then begin
    fCapacity := fCapacity * 2;
    SetLength( raw, fCapacity );
    SetLength( view, fCapacity );
    SetLength( drawn, fCapacity );
  end;

  raw[fCount - 1].x := x;
  raw[fCount - 1].y := y;

  Draw;
end;

constructor TSStorage.Create(waveLenScaler, apparatusScaler: TScaler);
begin
  fCapacity := 10;
  fCount := 0;

    SetLength( raw, fCapacity );
    SetLength( view, fCapacity );
    SetLength( drawn, fCapacity );

  fXScaler := waveLenScaler;
  fYScaler := apparatusScaler;
end;

procedure TSStorage.Draw;
var
  i : Integer;
  newP : TRealPoint;
begin
  for i := 0 to fCount -1 do begin
    if drawn[i] = false then begin
      newP.x := fXScaler.GetWave( raw[i].x );
      newP.y := fYScaler.GetWave( newP.x ) * ( raw[i].y - fDarkCurrent ); 

      ChartSeries.AddXY( newP.x, newP.y );

      view[i] := newP;
      drawn[i] := true;
    end;
  end;
end;

function TSStorage.GetPoint(i: Integer): TRealPoint;
begin
  result := view[i];
end;

function TSStorage.GetRawPoint(i: Integer): TRealPoint;
begin
  result := raw[i];
end;

procedure TSStorage.Recalculate;
var
  i : Integer;
begin
  for i := 0 to fCount - 1 do begin
    drawn[i] := false;
  end;

  ChartSeries.Clear;
  Draw;
end;

procedure TSStorage.setChartSeries(const Value: TChartSeries);
begin
  fChartSeries := Value;
  Recalculate;
end;

procedure TSStorage.setDarkCurrent(const Value: Real);
begin
  fDarkCurrent := Value;
  Recalculate;
end;

procedure TSStorage.setXScaler(const Value: TScaler);
begin
  fXScaler := Value;
  Recalculate;
end;

procedure TSStorage.setYScaler(const Value: TScaler);
begin
  fYScaler := Value;
  Recalculate;
end;

{ TSList }

function TSList.AddSpectrum(Item: TSStorage): Integer;
begin
  Add( Item );
end;

function TSList.GetSpectrum(ix: Integer): TSStorage;
begin
  result := TSStorage( Items[ix] );
end;

end.
